import PIL.Image
import numpy as np
import cv2
from tkinter import *
from PIL import Image,ImageTk,ImageDraw


width=200
height=200
black=(0,0,0)
white=(255,255,255)

def Run():
    img = cv2.imread("digits.png", 0)
    imgNhanDang = cv2.imread("anhso8.png", 0)

    cells = [np.hsplit(row, 100) for row in np.vsplit(img, 50)]
    # cv2.imwrite("anhsoaaa1.png",cells[10][10])
    arr = np.array(cells)
    arr2 = np.array(imgNhanDang)
    train = arr[:, :50].reshape(-1, 400).astype(np.float32)
    test = arr2.reshape(-1, 400).astype(np.float32)
    k = np.arange(10)
    train_labels = np.repeat(k, 250)[:, np.newaxis]

    knn = cv2.ml.KNearest_create()
    knn.train(train, 0, train_labels)
    kq1, kq2, kq3, kq4 = knn.findNearest(test, 10)
    box.insert(END,"Kết quả là:  {}".format(int(kq2)))
def XoaMH():
    box.delete(1.0,END)
    global image1,draw;
    cv.delete("all")
    image1=PIL.Image.new("RGB",(width,height),black)
    draw=ImageDraw.Draw(image1)
def SAVE():
    filename="img.png"
    image1.save(filename)
    img=Image.open("img.png")
    new_img=img.resize((20,20))
    new_img.save("anhso8.png")
def ve(event):
    x1,y1=(event.x-3),(event.y-3)
    x2,y2=(event.x+3),(event.y+3)
    cv.create_line(x1, y1, x2, y2, fill="white",width=15)
    draw.line([x1,y1,x2,y2],fill="white",width=15)
root=Tk()
root.geometry("1280x721+90+30")
root.title("Nhận diện chữ số viết tay")
root.iconbitmap("logo.ico")
cv=Canvas(root,width=width,height=height,bg="black")
cv.place(x=100,y=240)
image1=PIL.Image.new("RGB",(width,height),black)
draw=ImageDraw.Draw(image1)
cv.bind("<B1-Motion>",ve)
button_frame=Frame(root).pack(side=BOTTOM)
save_btn=Button(button_frame,text="Luu",command=SAVE)
save_btn.place(x=180,y=450)

box=Text(root,width=25,height=1)
box.pack(pady=300)

button_frame=Frame(root).pack(side=TOP)
run_btn=Button(button_frame,text="chay",command=Run)
run_btn.place(x=545,y=325)

button_frame=Frame(root).pack(side=BOTTOM)
xoa_btn=Button(button_frame,text="Xoa",command=XoaMH)
xoa_btn.place(x=650,y=325)

root.mainloop()